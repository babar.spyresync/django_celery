# Celery and Django to Handle Periodic Tasks

Django is one of the world's most popular web application frameworks.

Running periodic tasks is an important part of web application development. For example, you might want to periodically check all users for late payments, and email a polite reminder to users that haven't paid.

Django does not include any built-in functionality for handling periodic tasks. To perform periodic tasks, you'll need to rely on Celery.

What is Celery Celery is a background job manager that can be used with Python. Celery is compatible with several message brokers like RabbitMQ and Redis. The core Django framework does not provide the functionality to run periodic and automated background tasks. Celery comes into play in these situations allowing us to schedule tasks using an implementation called Celery Beat which relies on message brokers.

Some examples of scheduled tasks are

Batch email notifications,
Scheduled maintenance tasks,
Generating periodic reports,
Database and System snapshots
