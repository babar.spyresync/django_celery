# from __future__ import absolute_import, unicode_literals
# import os
# from celery import Celery
# from celery.schedules import crontab
#
# # set the default Django settings module for the 'celery' program.
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'simpletask.settings')
#
# app = Celery('simpletask')
#
# # Using a string here means the worker doesn't have to serialize
# # the configuration object to child processes.
# # - namespace='CELERY' means all celery-related configuration keys
# #   should have a `CELERY_` prefix.
#
# app.config_from_object('django.conf:settings', namespace='CELERY')
#
# # Load task modules from all registered Django app configs.
# app.autodiscover_tasks()
#
#
# @app.task(bind=True)
# def debug_task(self):
#     print("Request: {0!r}".format(self.request))
#
#
# app.conf.beat_schedule = {
#     # Scheduler Name
#     # 'print-message-ten-seconds': {
#     #     # Task Name (Name Specified in Decorator)
#     #     'task': 'print_msg_main',
#     #     # Schedule
#     #     'schedule': 10.0,
#     #     # Function Arguments
#     #     'args': ("Hello",)
#     # },
#     # # Scheduler Name
#     # 'print-time-twenty-seconds': {
#     #     # Task Name (Name Specified in Decorator)
#     #     'task': 'print_time',
#     #     # Schedule
#     #     'schedule': 20.0,
#     # },
#     # # Scheduler Name
#     # 'calculate-forty-seconds': {
#     #     # Task Name (Name Specified in Decorator)
#     #     'task': 'get_calculation',
#     #     # Schedule
#     #     'schedule': 40.0,
#     #     # Function Arguments
#     #     'args': (10, 20)
#     # },
#     # Execute the speed test every 10 mints
#     'network-speedtest-10min': {
#         'task': 'check_network_speed',
#         'schedule': crontab(minute='*/10')
#     }
# }


from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from celery.schedules import crontab
from  django.conf import settings

# Default Django settings module for Celery
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'simpletask.settings')

app = Celery('simpletask')

# Using a string here eliminates the need to serialize
# the configuration object to child processes by the Celery worker.

# - namespace='CELERY' means all celery-related configuration keys
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

# Load task modules from all registered Django applications.
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))


# app.conf.beat_schedule = {
#     # Execute the Speed Test every 10 minutes
#     'network-speedtest-10min': {
#         'task': 'check_network_speed',
#         'schedule': crontab(minute='*/10'),
#     },
# }
